<?php
require('config.inc.php');

Class SQL {
	
	private $GlobalError = array();
	
	function Connect() {
		global $mysql, $_connection;
		
		$_connection = @mysql_connect($mysql[hostname], $mysql[username], $mysql[password])
			or $this->AddToError('mysql_connect', ERR_CONNECT, mysql_errno());
		@mysql_select_db($mysql[database], $_connection)
			or $this->AddToError('mysql_select_db', ERR_DB, mysql_errno());
	}
	
	function Disconnect() {
		global $_connection;
	
		@mysql_close($_connection);
	}
	
	function RunSQL($SELECT, $debug=false) {
		global $_connection;
	
		$result = @mysql_query($SELECT, $_connection)
			or $this->AddToError('mysql_query', ERR_QUERY, mysql_errno());
		if (@mysql_num_rows($result) >= 1) {
			for ($i=0; $i<@mysql_num_rows($result); $i++)
				$return[] = @mysql_fetch_assoc($result);
		}
		else
			$return = false;
		return $return;
	}
	
	function AddToError($type, $error, $code=false) {
		if (!empty($type) || !empty($error)) {
			$x = count($this->GlobalError);
			$this->GlobalError[$x]['TYPE'] 	= $type;
			$this->GlobalError[$x]['ERROR']	= $error;
			if ($code)
				$this->GlobalError[$x]['CODE'] = $code;
		}
		return false;
	}
	
	function ShowGlobalError($die=false) {
		if (!empty($this->GlobalError)) {
			echo "<div id='error'>\n"
				." <ul>F�lgende er feil:\n";
			foreach ($this->GlobalError as $no) {
				foreach ($no as $key => $val) {
					echo $key=='ERROR' ? "  <li>$val <em>(#{$no['CODE']})</em></li>\n" : '';
				}
			}
			echo " </ul>\n"
				."</div>\n";
		}
		if ($die) {
			global $_connection;
			@mysql_close($_connection);
			Open('3');
			ReturnLinks();
			Close('3');
			Close();
			die();
		}
	}
}

class Tags {
	public $OpenedTags = array();

	function Open($tag=false, $class=false, $close=false) {
		if (!$tag) {
			echo "<?xml-stylesheet href='http://www.w3.org/StyleSheets/TR/W3C-REC.css' type='text/css'?><?xml version='1.0'?>\n"
				."<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//NO' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>\n"
				."<html xmlns='http://www.w3.org/1999/xhtml' lang='no'>\n"
				."<head>\n"
				." <meta http-equiv='Content-Type' content='application/xhtml+xml; charset=ISO-8859-1'>\n"
				." <meta name='copyright' content='� 2007 loketing.net' />\n"
				." <meta name='description' content='".TITLE."' />\n"
				." <meta name='version' content='".VERSION_NO."' />\n"
				." <title>".TITLE."</title>\n"
				." <link rel='stylesheet' type='text/css' href='stylesheet.css' title='stylesheet' media='all' />\n"
				." <style type='text/css'>\n"
				."  @import url(stylesheet.css);\n"
				." </style>\n"
				."<script type=\"text/javascript\">\n"
        ."  var _gaq = _gaq || [];\n"
        ."  _gaq.push(['_setAccount', 'UA-26316474-1']);\n"
        ."  _gaq.push(['_trackPageview']);\n"
        ."  (function() {\n"
        ."    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;\n"
        ."    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';\n"
        ."    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);\n"
        ."  })();\n"
        ."</script>\n"
				."</head>\n"
		
				."<body>\n";
		}
		else {
			/*echo "<$tag"
				.($attrs ? " $attrs" : '')
				.($close ? " /" : '')
				.">\n";
			if (!$close) 
				$this->OpenedTags[] = $tag;*/
			switch ($tag) {
				case '1':
					echo "<div id='top'".($class ? " class='$class'" : '').">\n";
					break;
				case '2':
					echo "<div id='content'".($class ? " class='$class'" : '').">\n";
					break;
				case '3':
					echo "<div id='footer'".($class ? " class='$class'" : '').">\n";
					break;
			}
					
		}
	}

	function Close($tag=false) {
		if (!$tag) {
			echo "<div id='ads'>\n";
			echo "<script type=\"text/javascript\"><!--\n"
          ."google_ad_client = \"ca-pub-0278804332824254\";\n"
          ."/* Tilfeldige ord */\n"
          ."google_ad_slot = \"1825449813\";\n"
          ."google_ad_width = 468;\n"
          ."google_ad_height = 60;\n"
          ."//-->\n"
          ."</script>\n"
          ."<script type=\"text/javascript\"\n"
          ."src=\"http://pagead2.googlesyndication.com/pagead/show_ads.js\">\n"
          ."</script>\n";
			echo "</div>";
			echo "</body>\n"
				."</html>";
			/*if (count($this->OpenedTags) != 0)
				echo "<span class='error'>".count($this->OpenedTags)." �pne tagger</span>\n";*/
		}
		else {
			/*echo "</$tag>\n";
			$key = array_search($tag, $this->OpenedTags);
			if ($key)
				unset($this->OpenedTags[$key]);*/
			switch ($tag) {
				case '1':
					echo "</div>\n";
					break;
				case '2':
					echo "</div>\n";
					break;
				case '3':
					echo "</div>\n";
					break;
			}
		}
	}
}

function Open() {
	echo "<?xml-stylesheet href='http://www.w3.org/StyleSheets/TR/W3C-REC.css' type='text/css'?><?xml version='1.0'?>\n"
		."<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//NO' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>\n"
		."<html xmlns='http://www.w3.org/1999/xhtml' lang='no'>\n"
		."<head>\n"
		." <meta http-equiv='Content-Type' content='application/xhtml+xml; charset=ISO-8859-1'>\n"
		." <meta name='copyright' content='� 2007 loketing.net' />\n"
		." <meta name='description' content='".TITLE."' />\n"
		." <title>".TITLE."</title>\n"
		." <link rel='stylesheet' type='text/css' href='stylesheet.css' title='stylesheet' media='all' />\n"
		." <style type='text/css'>\n"
		."  @import url(stylesheet.css);\n"
		." </style>\n"
		."</head>\n"
		
		."<body>\n";
}

function Close() {
	echo "</body>\n"
		."</html>";
}

function ReturnLinks() {
	global $link;
	$file = ReturnFullAddress();
	echo "<ul class='returnlinks'>\n";
	foreach ($link as $url => $desc) {
		echo ($url != $file ? "<li><a href='$url'>$desc</a></li>\n" : "<li>$desc</li>");
	}
	echo "</ul>\n";
	
	echo "<div class='footer'><p>NB! Dette skriptet er <em>langt</em> fra ferdig, og er her bare for underholdingens skyld.</p></div>";
}

function ReturnFullAddress () {
	$file = basename($_SERVER['PHP_SELF']).($_SERVER['QUERY_STRING'] ? '?'.$_SERVER['QUERY_STRING'] : '');
	return $file;
}

/*function Debug($var) {
	echo "<pre>\n";
	var_dump($var);
	echo "\n</pre>\n";
}*/

/*function Dump($var) { 
	if (key_exists($var, $GLOBALS)) {
		echo "<pre>$$var:\n";
		var_dump($GLOBALS[$var]);
		echo "\n</pre>\n";
	}
	else
		echo "<span class='error'>No variables named <em>$$var</em>!</span>\n";
}*/

function Dump(&$var, $scope=false, $prefix='unique', $suffix='value') { // http://no.php.net/manual/en/language.variables.php#49997
	if($scope) 	$vals = $scope;
	else		$vals = $GLOBALS;
	$old = $var;
	$var = $new = $prefix.rand().$suffix;
	$vname = FALSE;
	foreach($vals as $key => $val)
		if($val === $new) $vname = $key;
	$var = $old;
	
	if ($vname)
		echo "<pre><b>$$vname:</b>\n";
	else 
		echo "<pre><b>Unknown variable:</b>\n";
	var_dump($var);
	echo "</pre>\n";
}



?>
