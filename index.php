<?php
require_once('config.inc.php');
require_once('core/functions.php');

// Enable CORS
//header("Access-Control-Allow-Origin: *");

$sql = new SQL();
$tags = new Tags();
$tags->Open();
$sql->Connect();

$SELECT = "(SELECT CONVERT(ord USING latin1) AS ord FROM $substantiv WHERE "
		.(!$_GET['ord'] ? '1 ' : "ord != '".mysql_real_escape_string($_GET['ord'])."' ")
		."ORDER BY RAND() LIMIT "
		.(!$_GET['ord'] ? "0,2)" : 	 "0,1) UNION ALL (SELECT CONVERT(ord USING latin1) AS ord FROM $substantiv "
									."WHERE ord ='".mysql_real_escape_string($_GET['ord'])."' LIMIT 0 , 1)");
$result = $sql->RunSQL($SELECT, true)
	or $sql->ShowGlobalError(true);

$ord = array($result[0]['ord'], $result[1]['ord']); // simplifies further processing
shuffle($ord);

if (substr($ord[0], -2, 1) == substr($ord[0], -1, 1) && substr($ord[0], -1, 1) == substr($ord[1], 0, 1)) {
	#$ord['new'] = ucfirst(substr($ord[0],0,-1).$ord[1]);
	$ord[0] = substr($ord[0],0,-1);
}
/*else {
	$ord['new'] = ucfirst($ord[0].$ord[1]);
}*/
if ($_GET['ord'] && (is_null($ord[0]) || is_null($ord[1]))) {
	$sql->AddToError('SELECT', "Ordet <em>{$_GET['ord']}</em> finnes ikke i databasen.");
	$sql->ShowGlobalError(true);
}
$ord['new'] = ucfirst($ord[0].$ord[1]);
$ord['full'] = "<a href='?ord=".htmlentities($ord[0])."'>".ucfirst($ord[0])."</a><a href='?ord=".htmlentities($ord[1])."'>{$ord[1]}</a>";

$tags->Open('1');
echo "<h1>{$ord['full']}</h1>\n"
	."<p class='all'><a href='".ReturnFullAddress()."'>Nytt ord</a>"
	.($_GET['ord'] ? " - <a href='{$_SERVER['PHP_SELF']}'>Tilbakestill</a>" : '')
	."</p>\n";
$tags->Close('1');
$tags->Open('2');

#Dump('SELECT');
#Dump('ord');
#echo vname($SELECT);
#Dump($SELECT);
#Dump($ord);

$sql->Disconnect();
$tags->Close('2');
$tags->Open('3');
ReturnLinks();
$tags->Close('3');
$tags->Close();

?>
