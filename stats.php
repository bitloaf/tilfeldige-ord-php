<?php

require('config.inc.php');
require('core/functions.php');

$sql = new SQL();
$tags = new Tags();
$tags->Open();
$sql->Connect();

$SELECT = "SELECT COUNT(ord) as x FROM $substantiv";
$result = $sql->RunSQL($SELECT, true)
	or $sql->ShowGlobalError(true);

$x 		= $result[0]['x'];
$xtot 	= $x*($x-1);
$xtot = number_format($xtot,0,'',' ');

$tags->Open('1');
echo "<h1>Statistikk:</h1>\n";
$tags->Close('1');

$tags->Open('2');
echo "<center>\n";
echo "<table>\n"
	." <tr><td>Antall ord:</td><td>$x</td></tr>\n"
	." <tr><td>Antall mulige kombinasjoner:</td><td>$xtot</td></tr>\n"
	." <tr><td></td><td></td></tr>\n"
	."</table>\n";
echo "</center>\n";

$sql->Disconnect();
$tags->Close('2');

$tags->Open('3');
ReturnLinks();
$tags->Close('3');
$tags->Close();
?>
