<?php
require('config.inc.php');
require('core/functions.php');

$tags = new Tags();
$tags->Open();
$sql = new SQL();
$sql->Connect();

switch ($_GET['show']) {
	case 'fav':
		$SELECT = "SELECT ord FROM $favoritter WHERE 1 ORDER BY RAND()";
		break;
	default:
		$SELECT = "SELECT ord FROM $substantiv as substantiv WHERE 1 ORDER BY ord";
		break;
}
$result = $sql->RunSQL($SELECT)
	or $sql->ShowGlobalError(true);

$tags->Open('1');
echo ($_GET['show'] == 'fav' ? "<h1>Favorittord:</h1>\n" : "<h1>Alle ord:</h1>\n");
$tags->Close('1');

$tags->Open('2');
echo "<p class='all'>";
for ($i=0; $i<count($result); $i++) {
	if ($_GET['show'] == 'fav')
		echo ($i==0 ? ucfirst($result[$i]['ord']).', ' : ($i==count($result)-1 ? "og ".strtolower($result[$i]['ord']).'.' : strtolower($result[$i]['ord']).', '));
	else 
		echo ($i==0 ? "<a href='index.php?ord={$result[$i]['ord']}'>".ucfirst($result[$i]['ord']).'</a>, ' : ($i==count($result)-1 ? "og <a href='index.php?ord={$result[$i]['ord']}'>".strtolower($result[$i]['ord']).'</a>.' : "<a href='index.php?ord={$result[$i]['ord']}'>".strtolower($result[$i]['ord']).'</a>, '));
}
echo "</p>\n<h3>Antall ord: ".count($result)."</h3>\n";
$tags->Close('2');

$sql->Disconnect();
$tags->Open('3');
ReturnLinks();
$tags->Close('3');
$tags->Close();
?>